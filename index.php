<?php 
    error_reporting(0);

    if(isset($_POST['submit'])){
        $kalimat = $_POST['kalimat'];
        $kalimat = strtolower($kalimat);
        $hasil = palindrom($kalimat);
    }
    function palindrom ($kalimat) {
        $kata = explode(" ",$kalimat);   
        $hitung = 0;
        $kata_palindrom = null;

        for ($i=0; $i < count($kata); $i++) { 
            for($j=count($kata); $j >= 0; $j--){
                if ($kata[$i] == strrev($kata[$j])) {
                    // $kata_palindrom .= "<br>-".$kata[$i];
                    $kata_palindrom[] = $kata[$i];
                    $hitung++;
                }
            }
        }
        return 'Kata palindrom berjumlah : '.$hitung.' <br> kata palindrom pertama adalah : '.$kata_palindrom[0];  
    }    
?>
<html>
    <body>
        <form action="" method="POST">
            <input type="text" name="kalimat" required value="<?echo $kalimat;?>">
            <button name="submit" >CARI KATA</button>
        </form>
        <br>
        <p><?php echo $hasil;?></p>
    </body>
</html>